export default [
  { text: "Low", abbreviated: "Low", slug: "low", id: "risk0", color: "var(--risk0)" },
  { text: "Medium", abbreviated: "Med", slug: "medium", id: "risk1", color: "var(--risk1)" },
  { text: "High", abbreviated: "High", slug: "high", id: "risk2", color: "var(--risk2)" },
];
